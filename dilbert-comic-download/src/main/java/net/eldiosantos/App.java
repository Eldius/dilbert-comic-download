package net.eldiosantos;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    private final String DEST_FOLDER = "./../img";
    private final String url;

    public App(String url) {
        this.url = url;
    }


    public void downloadAll() throws IOException {

        String next = new StringBuffer(url)
                .append("/strip/")
                .append(new SimpleDateFormat("yyyy-MM-dd").format(new Date())).toString();

        downloadAllRecursive(next);

    }

    private void downloadAllRecursive(final String next) throws IOException {
        System.out.println("location: " + next);
        final Document doc = getDocument(next);
        downloadComic(this.getImageUrl(doc));
        downloadAllRecursive(url + getNextLink(doc));
    }

    public void downloadComic(final String imgUrl) throws IOException {
        final String header = "Content-Disposition";

        FileUtils.touch(new File(DEST_FOLDER + File.separator + ".destFolder"));

        //final HttpHost proxy = new HttpHost("127.0.0.1", 3128, "http");

        //final HttpClient client = HttpClientBuilder.create().setProxy(proxy).build();
        final HttpClient client = HttpClientBuilder.create().build();
        final HttpGet request = new HttpGet(imgUrl);

        request.addHeader("User-Agent", "Eldius' Web Browser");

        final HttpResponse response = client.execute(request);

        final Header contentDisposition = response.getFirstHeader("Content-Disposition");
        final String[] split = contentDisposition.getValue().split("\"");
        final String fileName = "img_" + split[split.length - 1];
        IOUtils.copy(response.getEntity().getContent(), new FileOutputStream(DEST_FOLDER + File.separator + fileName));
    }

    private String getNextLink(final Document doc) {
        return doc.select(".nav-comic").get(0).children().attr("href");
    }

    private String getImageUrl(final Document doc) throws IOException {
        final Elements img = doc.select(".img-comic");
        final String imageSource = img.attr("src");
        if (!imageSource.toLowerCase().startsWith("http")) {
            return "http:" + imageSource;
        }
        return imageSource;
    }

    private Document getDocument(String baseUrl) throws IOException {
        return Jsoup.parse(new URL(baseUrl), 90000);
    }

    public static void main( String[] args ) throws IOException {
        final String baseUrl = "http://dilbert.com";
        final App app = new App(baseUrl);
        app.downloadAll();
    }

}
